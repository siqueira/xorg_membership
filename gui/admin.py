from django.contrib import admin
from .models import Link, Profile, MembershipPeriod, Membership


class LinkModel(admin.ModelAdmin):
    list_display = ('short_name', )
    search_fields = ['short_name', 'description']


admin.site.register(Link, LinkModel)


class ProfileModel(admin.ModelAdmin):
    list_display = ('user', )
    search_fields = ['user', 'employer']


admin.site.register(Profile, ProfileModel)


class MembershipPeriodModel(admin.ModelAdmin):
    list_display = ('short_name', 'start')
    search_fields = ['short_name', 'description']


admin.site.register(MembershipPeriod, MembershipPeriodModel)


class MembershipModel(admin.ModelAdmin):
    list_display = ('user_profile', 'period', 'applied_on', 'approved_on', 'rejected_on')
    search_fields = ['user_profile__user__first_name', 'user_profile__user__last_name',
                     'period__short_name', 'rejection_reason']


admin.site.register(Membership, MembershipModel)
