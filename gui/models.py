from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.functional import cached_property
from django.utils import timezone


class Link(models.Model):
    short_name = models.CharField(max_length=100,
                                  help_text="Short name to describe the link")
    description = models.CharField(max_length=255, blank=True, null=True,
                                   help_text="Longer description")
    index_position = models.IntegerField(help_text="Smaller index means it will "
                                                   "be higher in the list",
                                         default=0)
    url = models.URLField(help_text="Path to the document/website you want to link")


class MembershipPeriod(models.Model):
    short_name = models.CharField(max_length=50, unique=True,
                                  help_text="Name/title of the period. Eg. '2017-2018'")

    description = models.TextField(help_text="Description of what the period represents. Will be "
                                             "displayed when prompting users to re-new their membership")

    agreement_url = models.URLField(help_text="URL to the membership agreement")

    start = models.DateTimeField(help_text="Date at which the new membership period starts.")

    @classmethod
    def current_period(cls):
        return MembershipPeriod.objects.exclude(start__gt=timezone.now()).order_by("start").last()

    @property
    def members(self):
        return Membership.objects.filter(period=self).exclude(approved_on=None, rejected_on=None)

    def __str__(self):
        return "period '{}' starting on {}".format(self.short_name, self.start)


class Membership(models.Model):
    period = models.ForeignKey(MembershipPeriod, on_delete=models.CASCADE)
    user_profile = models.ForeignKey("Profile", on_delete=models.CASCADE)

    applied_on = models.DateTimeField(auto_now_add=True)
    approved_on = models.DateTimeField(null=True, blank=True, db_index=True)
    rejected_on = models.DateTimeField(null=True, blank=True, db_index=True)
    rejection_reason = models.TextField(null=True, blank=True, help_text="Reason for the rejection")

    class Meta:
        unique_together = ('period', 'user_profile')

    @classmethod
    def pending_memberships(self):
        return Membership.objects.filter(approved_on=None, rejected_on=None).order_by('id')

    @property
    def is_pending(self):
        return self.approved_on is None and self.rejected_on is None

    @property
    def is_approved(self):
        return self.approved_on is not None and self.rejected_on is None

    @property
    def is_rejected(self):
        return self.rejected_on is not None

    @property
    def status(self):
        if self.is_pending:
            return "Pending approval"
        elif self.is_approved:
            return "Approved on {}".format(self.approved_on.date())
        elif self.is_rejected:
            return "Rejected on {}".format(self.rejected_on.date())
        else:
            raise ValueError("Unknown state")


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    memberships = models.ManyToManyField(MembershipPeriod, through="Membership")

    employer = models.CharField(max_length=80, blank=True,
                                help_text="Current employer (leave empty for hobbyists or "
                                          "if you want to keep this private)")
    public_statement = models.TextField(blank=True,
                                        help_text="This information will be displayed to other members")

    @classmethod
    def existing_employers(cls):
        return sorted(set([e[0] for e in Profile.objects.values_list('employer') if len(e[0]) > 0]))

    @receiver(post_save, sender=User)
    def create_member(sender, instance, created, **kwargs):
        if created:
            Profile.objects.create(user=instance)

    @receiver(post_save, sender=User)
    def save_member(sender, instance, **kwargs):
        instance.profile.save()

    @cached_property
    def membership(self):
        cur_period = MembershipPeriod.current_period()
        if cur_period is None:
            return None
        return Membership.objects.filter(user_profile=self, period=cur_period).first()

    @cached_property
    def last_membership(self):
        return self.memberships.exclude(membership__approved_on=None).last()

    def __str__(self):
        return "{} <{}>".format(self.user.get_full_name(), self.user.email)
