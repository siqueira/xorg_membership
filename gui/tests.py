from django.conf import settings
from django.contrib.auth import get_user_model
from django.test import TestCase
from django.utils import timezone
from django.urls import reverse
from datetime import timedelta
from unittest.mock import patch, MagicMock, PropertyMock

from allauth.account.models import EmailAddress

from htmlvalidator.client import ValidatingClient

from .models import MembershipPeriod, Membership, Link, Profile
from .context_processors import admin_tasks_count, current_period
from .forms import ProfileForm, MembershipApplicationForm
from .views import render

import os


def create_user_and_log_in(client, admin=False, verified=False):
    user = get_user_model().objects.create_user('user', 'user@provider.com', 'pwd', is_superuser=admin)
    EmailAddress.objects.create(user=user, primary=True, verified=verified,
                                email=user.email)
    client.login(username='user', password='pwd')
    return user


class MembershipPeriodTests(TestCase):
    def test_current_period(self):
        # Check that by default, we get no periods at all
        self.assertEqual(MembershipPeriod.current_period(), None)

        # Check that no future period becomes now
        MembershipPeriod.objects.create(short_name="future", description="", agreement_url="https://x.org",
                                        start=timezone.now() + timedelta(hours=1))
        self.assertEqual(MembershipPeriod.current_period(), None)

        # Check that the ordering is right for dates in the past
        old = MembershipPeriod.objects.create(short_name="old", description="", agreement_url="https://x.org",
                                              start=timezone.now())
        self.assertEqual(MembershipPeriod.current_period(), old)

        new = MembershipPeriod.objects.create(short_name="new", description="", agreement_url="https://x.org",
                                              start=timezone.now())

        self.assertEqual(MembershipPeriod.current_period(), new)

    @patch('gui.models.Membership.objects.filter')
    def test_members(self, filter_mocked):
        period = MembershipPeriod.objects.create(short_name="period name", start=timezone.now())

        members = period.members
        filter_mocked.assert_called_with(period=period)
        filter_mocked.return_value.exclude.assert_called_with(approved_on=None, rejected_on=None)
        self.assertEqual(members, filter_mocked.return_value.exclude.return_value)

    def test_str(self):
        start = timezone.now()
        period = MembershipPeriod.objects.create(short_name="period name", start=start)
        self.assertEqual(str(period), "period 'period name' starting on {}".format(start))


class MembershipTests(TestCase):
    def test_empty(self):
        membership = Membership()
        self.assertTrue(membership.is_pending)
        self.assertFalse(membership.is_approved)
        self.assertFalse(membership.is_rejected)
        self.assertEqual(membership.status, "Pending approval")

    def test_approved(self):
        membership = Membership(approved_on=timezone.now())
        self.assertFalse(membership.is_pending)
        self.assertTrue(membership.is_approved)
        self.assertFalse(membership.is_rejected)
        self.assertEqual(membership.status, "Approved on {}".format(membership.approved_on.date()))

    def test_rejected(self):
        membership = Membership(rejected_on=timezone.now())
        self.assertFalse(membership.is_pending)
        self.assertFalse(membership.is_approved)
        self.assertTrue(membership.is_rejected)
        self.assertEqual(membership.status, "Rejected on {}".format(membership.rejected_on.date()))

    def test_approved_and_rejected(self):
        membership = Membership(approved_on=timezone.now(), rejected_on=timezone.now())
        self.assertFalse(membership.is_pending)
        self.assertFalse(membership.is_approved)
        self.assertTrue(membership.is_rejected)
        self.assertEqual(membership.status, "Rejected on {}".format(membership.rejected_on.date()))

    @patch('gui.models.Membership.objects.filter')
    def test_members(self, filter_mocked):
        memberships = Membership.pending_memberships()
        filter_mocked.assert_called_with(approved_on=None, rejected_on=None)
        filter_mocked.return_value.order_by.assert_called_with('id')
        self.assertEqual(memberships, filter_mocked.return_value.order_by.return_value)

    @patch('gui.models.Membership.is_pending', new_callable=PropertyMock)
    @patch('gui.models.Membership.is_approved', new_callable=PropertyMock)
    @patch('gui.models.Membership.is_rejected', new_callable=PropertyMock)
    def test_unknown_state(self, rejected_mock, approved_mock, pending_mock):
        rejected_mock.return_value = False
        approved_mock.return_value = False
        pending_mock.return_value = False

        membership = Membership(approved_on=timezone.now(), rejected_on=timezone.now())
        with self.assertRaises(ValueError, msg="Unknown state"):
            membership.status


class ProfileTests(TestCase):
    def create_user(self, username, first_name="First", last_name="Last", email='a@x.org',
                    employer='', public_statement=''):
        user = get_user_model().objects.create(username=username, first_name="First",
                                               last_name="Last", email='a@x.org')
        user.profile.employer = employer
        user.public_statement = public_statement
        user.save()
        return user

    def setUp(self):
        self.user = self.create_user(username='user')
        self.profile = self.user.profile

    def test_membership__no_period(self):
        self.assertEqual(self.profile.membership, None)

    def test_membership__with_active_period_but_no_membership_application(self):
        MembershipPeriod.objects.create(start=timezone.now() - timedelta(hours=1))
        self.assertEqual(self.profile.membership, None)

    def test_membership__with_active_period_and_membership_application(self):
        period = MembershipPeriod.objects.create(start=timezone.now() - timedelta(hours=1))
        membership = Membership.objects.create(period=period, user_profile=self.profile)
        self.assertEqual(self.profile.membership, membership)

    def test_last_membership__no_periods(self):
        self.assertEqual(self.profile.last_membership, None)

    def test_last_membership__with_periods(self):
        period1 = MembershipPeriod.objects.create(short_name="p1", start=timezone.now())
        period2 = MembershipPeriod.objects.create(short_name="p2", start=timezone.now())
        period3 = MembershipPeriod.objects.create(short_name="p3", start=timezone.now())

        # Create 3 memberships, but the last one pending approval by administrators
        Membership.objects.create(period=period1, user_profile=self.profile, approved_on=timezone.now())
        Membership.objects.create(period=period2, user_profile=self.profile, approved_on=timezone.now())
        Membership.objects.create(period=period3, user_profile=self.profile)

        self.assertTrue(self.profile.last_membership, period2)

    def test_str(self):
        self.assertEqual(str(self.profile), "First Last <a@x.org>")

    def test_existing_employers(self):
        for i, employer in enumerate(['Intel', 'Samsung', 'Suse', 'Red Hat', '', 'Intel',
                                      'Canonical']):
            self.create_user(username="user{}".format(i), employer=employer)

        self.assertEqual(Profile.existing_employers(),
                         ['Canonical', 'Intel', 'Red Hat', 'Samsung', 'Suse'])


class CP_AdminTasksCountTests(TestCase):
    @patch('gui.models.Membership.pending_memberships',
           return_value=MagicMock(count=MagicMock(return_value=42)))
    def test_admin_tasks_count__normal_user(self, pending_memberships_mock):
        request = MagicMock(user=MagicMock(is_superuser=False))
        self.assertEqual(admin_tasks_count(request),
                         {"admin_tasks_count": 0})

    @patch('gui.models.Membership.pending_memberships',
           return_value=MagicMock(count=MagicMock(return_value=42)))
    def test_admin_tasks_count__superuser(self, pending_memberships_mock):
        request = MagicMock(user=MagicMock(is_superuser=True))
        self.assertEqual(admin_tasks_count(request),
                         {"admin_tasks_count": 42})


class CP_CurrentPeriodTests(TestCase):
    @patch('gui.models.MembershipPeriod.current_period',
           return_value=MagicMock())
    def test_current_period(self, current_period_mock):
        self.assertEqual(current_period(None),
                         {"current_membership_period": current_period_mock.return_value})
        current_period_mock.assert_called_with()


class ProfileFormTests(TestCase):
    def test_empty_form(self):
        form = ProfileForm({})

        user = MagicMock()
        form.save(user)

        self.assertFalse(form.is_valid())
        user.save.assert_not_called()

    def test_all_but_employer(self):
        form = ProfileForm({'first_name': 'First', 'last_name': 'Last', 'public_statement': 'My statement'})

        user = MagicMock()
        form.save(user)

        self.assertTrue(form.is_valid())
        self.assertEqual(user.first_name, 'First')
        self.assertEqual(user.last_name, 'Last')
        self.assertEqual(user.profile.employer, '')
        self.assertEqual(user.profile.public_statement, 'My statement')
        user.save.assert_called_with()


class MembershipApplicationFormTests(TestCase):
    def setUp(self):
        self.user = get_user_model().objects.create(first_name="First", last_name="Last", email='a@x.org')
        self.period = MembershipPeriod.objects.create(start=timezone.now())

    @patch('gui.models.Membership.objects.create')
    def test_empty_form(self, create_mock):
        form = MembershipApplicationForm({})
        self.assertFalse(form.save())

        self.assertFalse(form.is_valid())
        create_mock.assert_not_called()

    @patch('gui.models.Membership.objects.create')
    def test_not_agreeing_to_membership(self, create_mock):
        form = MembershipApplicationForm({"period_id": self.period.id, "user_id": self.user.id,
                                          "public_statement": "My statement", "agree_membership": False})
        self.assertFalse(form.save())

        self.assertFalse(form.is_valid())
        self.assertEqual(get_user_model().objects.get(pk=self.user.id).profile.public_statement, "")
        create_mock.assert_not_called()

    @patch('gui.models.Membership.objects.create')
    def test_all_valid(self, create_mock):
        form = MembershipApplicationForm({"period_id": self.period.id, "user_id": self.user.id,
                                          "public_statement": "My statement", "agree_membership": True})
        self.assertTrue(form.save())

        self.assertTrue(form.is_valid(), form.errors)
        self.assertEqual(get_user_model().objects.get(pk=self.user.id).profile.public_statement, "My statement")
        create_mock.assert_called_with(period=self.period, user_profile=self.user.profile)


# Make sure that all the context processors are included
class ViewRenderTests(TestCase):
    @patch("gui.views.django_render")
    def test_render(self, django_render_mocked):
        request = MagicMock(user=MagicMock(is_superuser=True))

        expected = {'admin_tasks_count': 0, 'current_membership_period': None}

        self.assertEqual(render(request, "template_path", {}), django_render_mocked.return_value)
        django_render_mocked.assert_called_with(request, "template_path", expected)


class GuiViewMixin:
    def setUpGuiTests(self, url, verified_user_needed=False, superuser_needed=False):
        self.url = url
        self.verified_user_needed = verified_user_needed or superuser_needed
        self.superuser_needed = superuser_needed

        if os.environ.get('VALIDATE_HTML') is not None:
            self.client = ValidatingClient()  # pragma: no cover

        # HACK: Massively speed up the login primitive. We don't care about security in tests
        settings.PASSWORD_HASHERS = ('django.contrib.auth.hashers.MD5PasswordHasher', )

    def create_user_and_log_in(self, admin=False, verified=False):
        return create_user_and_log_in(self.client, verified=verified, admin=admin)

    def do_POST(self, params):
        response = self.client.post(self.url, params)
        self.assertEqual(response.status_code, 302)
        return self.client.get(self.url, {})

    def test_unauthenticated_user(self):
        response = self.client.get(self.url)

        if self.verified_user_needed:
            self.assertEqual(response.status_code, 302)
        else:
            self.assertEqual(response.status_code, 200)

        # Base.html checks
        if not self.verified_user_needed:
            self.assertContains(response, ">Sign In</a>")
            self.assertContains(response, ">Sign Up</a>")
            self.assertNotContains(response, '>Admin <span class="badge">')

        self.additional_unauthenticated_checks(response)

    def test_logged_in_unverified_user(self):
        # make sure no email is getting send, which slows down tests
        settings.EMAIL_BACKEND = 'django.core.mail.backends.dummy.EmailBackend'

        self.create_user_and_log_in(admin=False, verified=False)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)

        # Base.html checks
        if not self.verified_user_needed:
            self.assertContains(response, ">Your Profile</a>")
            self.assertContains(response, ">Change E-mail</a>")
            self.assertContains(response, ">Sign Out</a>")
            self.assertNotContains(response, '>Admin <span class="badge">')

            self.additional_logged_in_user_checks(response)

        return response

    def test_logged_in_verified_user(self):
        self.create_user_and_log_in(admin=False, verified=True)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 403 if self.superuser_needed else 200)

        # Base.html checks
        if not self.superuser_needed:
            self.assertContains(response, ">Your Profile</a>")
            self.assertContains(response, ">Change E-mail</a>")
            self.assertContains(response, ">Sign Out</a>")
            self.assertNotContains(response, '>Admin <span class="badge">')

        self.additional_logged_in_user_checks(response)

        return response

    @patch('gui.models.Membership.pending_memberships',
           return_value=MagicMock(count=MagicMock(return_value=42)))
    def test_logged_in_admin(self, admin_tasks_count_mocked):
        self.create_user_and_log_in(admin=True, verified=True)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)

        # Base.html checks
        self.assertContains(response, ">Your Profile</a>")
        self.assertContains(response, ">Change E-mail</a>")
        self.assertContains(response, ">Sign Out</a>")
        self.assertContains(response, '>Admin <span class="badge">')
        self.assertContains(response, '>Approve memberships <span class="badge">42</span>')

        self.additional_admin_checks(response)

        return response

    # To be overriden
    def additional_unauthenticated_checks(self, response):
        pass

    def additional_logged_in_user_checks(self, response):
        pass

    def additional_admin_checks(self, response):
        pass


class ViewIndexTests(TestCase, GuiViewMixin):
    def setUp(self):
        self.setUpGuiTests(reverse('index'))

    def test_check_links(self):
        # Create a set of links and their corresponding HTML
        links = []
        for i in range(0, 10):
            short_name = "Link_{}".format(i)
            url = "http://link-{}.com".format(i)
            Link.objects.create(short_name=short_name, index_position=10-i, url=url,
                                description="My desc")
            links.append('<a href="{}" title="My desc">{}</a>'.format(url, short_name))

        # Now check that everything is alright
        response = self.client.get(reverse('index'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(list(response.context['links']),
                         list(Link.objects.all().order_by("index_position")))
        for link in links:
            self.assertContains(response, link)

    def additional_unauthenticated_checks(self, response):
        self.assertContains(response, "Log-in or Sign-up")

    def additional_logged_in_user_checks(self, response):
        self.assertContains(response, 'role="button">Apply</a>')

    def additional_admin_checks(self, response):
        self.additional_logged_in_user_checks(response)

    def test_user_with_pending_membership(self):
        user = self.create_user_and_log_in(verified=True)
        period = MembershipPeriod.objects.create(short_name="2018-2019", start=timezone.now())
        Membership.objects.create(period=period, user_profile=user.profile)

        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Thanks for applying, your application is being processed!")

    def test_user_with_active_membership(self):
        user = self.create_user_and_log_in(verified=True)
        period = MembershipPeriod.objects.create(short_name="2018-2019", start=timezone.now())
        Membership.objects.create(period=period, user_profile=user.profile, approved_on=timezone.now())

        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Your application has been approved, thanks for being a member!")

    def test_user_with_rejected_membership(self):
        user = self.create_user_and_log_in(verified=True)
        period = MembershipPeriod.objects.create(short_name="2018-2019", start=timezone.now())
        Membership.objects.create(period=period, user_profile=user.profile,
                                  rejected_on=timezone.now(), rejection_reason="You\nare\na\ntroll")

        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "<p>Your application has been rejected. Reason:</p>")
        self.assertContains(response, "<p>You<br>are<br>a<br>troll</p>")


class ViewMembersTests(TestCase, GuiViewMixin):
    def setUp(self):
        self.setUpGuiTests(reverse('members'), verified_user_needed=True)

    @patch('gui.models.MembershipPeriod.members')
    def test_no_members(self, members_mock):
        MembershipPeriod.objects.create(short_name="2018-2019", start=timezone.now())

        response = self.test_logged_in_verified_user()

        # Check that the members list has been generated with the right calls
        members_mock.order_by.assert_called_with("user_profile__user__last_name")
        members_mock.order_by.return_value.select_related.assert_called_with('user_profile',
                                                                             'user_profile__user')
        self.assertEqual(response.context['object_list'],
                         members_mock.order_by.return_value.select_related.return_value)

        self.assertContains(response, "No members yet.")

    def __setup_db(self):
        period = MembershipPeriod.objects.create(short_name="2018-2019", start=timezone.now())
        for i in range(0, 3):
            username = "user_{}".format(i)
            user = get_user_model().objects.create(first_name=username, last_name="last",
                                                   username=username)
            user.is_superuser = (i == 1)
            user.profile.employer = "" if i == 0 else "Intel"
            user.profile.public_statement = "My public\nstatement"
            user.save()
            Membership.objects.create(period=period, user_profile=user.profile, approved_on=timezone.now())
        return period

    def test_with_members_as_member(self):
        self.__setup_db()

        response = self.test_logged_in_verified_user()
        self.assertContains(response, "<td>last, user_0</td>")
        self.assertContains(response, "<td>last, user_1</td>")
        self.assertContains(response, "<td>last, user_2</td>")
        self.assertContains(response, "<td>Unknown</td>")
        self.assertContains(response, "<td>Intel</td>")
        self.assertContains(response, "<td><p>My public<br>statement</p></td>")

        # Verify that the administrator-only actions are not visible
        self.assertNotContains(response, "<th>Action</th>")
        self.assertNotContains(response, 'value="Make Admin"')
        self.assertNotContains(response, 'value="Make User"')
        self.assertNotContains(response, "No actions available")

    def test_with_members_as_admin(self):
        period = self.__setup_db()

        # Verify that the administrator-only actions are visible
        user = create_user_and_log_in(self.client, admin=True, verified=True)
        response = self.client.get(self.url)
        self.assertContains(response, "<th>Action</th>")
        self.assertContains(response, 'value="Make Admin"')
        self.assertContains(response, 'value="Make User"')
        self.assertContains(response, '<form action="/members/1/make/admin" method="post">')
        self.assertContains(response, '<form action="/members/2/make/user" method="post">')
        self.assertContains(response, '<form action="/members/3/make/admin" method="post">')
        self.assertNotContains(response, '<form action="/members/{}/make/user" method="post">'.format(user.id))
        self.assertNotContains(response, "No actions available")

        # Make the current user a member and test that we cannot perform any action on him
        Membership.objects.create(period=period, user_profile=user.profile,
                                  approved_on=timezone.now())
        response = self.client.get(self.url)
        self.assertContains(response, "No actions available")
        self.assertNotContains(response, '<form action="/members/{}/make/user" method="post">'.format(user.id))


class ViewChangeUserStatus(TestCase):
    def setUp(self):
        self.user = get_user_model().objects.create_user('not_admin', 'user@provider.com', 'pwd',
                                                         is_superuser=False)

    def test_get_and_admin(self):
        create_user_and_log_in(self.client, admin=True, verified=True)
        response = self.client.get(reverse('members-user-to-admin', kwargs={"pk": self.user.id}))
        self.assertEqual(response.status_code, 405)

    def test_non_admin(self):
        create_user_and_log_in(self.client, admin=False, verified=True)
        response = self.client.post(reverse('members-user-to-admin', kwargs={"pk": self.user.id}))
        self.assertEqual(response.status_code, 403)

    def test_admin_post_with_referrer(self):
        create_user_and_log_in(self.client, admin=True, verified=True)
        referrer = 'http://my.website.com/foo/bar'

        self.assertFalse(get_user_model().objects.get(pk=self.user.id).is_superuser)
        response = self.client.post(reverse('members-user-to-admin', kwargs={"pk": self.user.id}),
                                    {}, HTTP_REFERER=referrer)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, referrer)
        self.assertTrue(get_user_model().objects.get(pk=self.user.id).is_superuser)

        response = self.client.post(reverse('members-admin-to-user', kwargs={"pk": self.user.id}),
                                    {}, HTTP_REFERER=referrer)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, referrer)
        self.assertFalse(get_user_model().objects.get(pk=self.user.id).is_superuser)

    def test_admin_change_its_own_rights(self):
        user = create_user_and_log_in(self.client, admin=True, verified=True)
        response = self.client.post(reverse('members-admin-to-user', kwargs={"pk": user.id}))
        self.assertEqual(response.status_code, 302)
        self.assertTrue(get_user_model().objects.get(pk=user.id).is_superuser)


class ViewProfileTests(TestCase, GuiViewMixin):
    def setUp(self):
        self.setUpGuiTests(reverse('account-profile'), verified_user_needed=True)

    def test_post_invalid_form(self):
        self.create_user_and_log_in(verified=True)
        response = self.do_POST({})
        self.assertContains(response, "Your profile is invalid")

    def test_post_valid_form(self):
        self.create_user_and_log_in(verified=True)
        response = self.do_POST({"first_name": "First",
                                 "last_name": "Last",
                                 "public_statement": "Statement"})
        self.assertContains(response, "Your profile was updated successfully")

    @patch('gui.models.Profile.existing_employers', return_value=['Collabora', 'Intel',
                                                                  'Google', 'Samsung'])
    def test_existing_employers_list(self, existing_employers_mock):
        self.create_user_and_log_in(verified=True)
        response = self.client.get(self.url)

        self.assertContains(response, '<datalist id="list__employers-list"><option value="Collabora">'
                            '<option value="Intel"><option value="Google"><option value="Samsung"></datalist>')


class ViewMembershipApplicationTests(TestCase, GuiViewMixin):
    def setUp(self):
        self.setUpGuiTests(reverse('membership-application'), verified_user_needed=True)

    def test_no_periods(self):
        response = self.test_logged_in_admin()
        self.assertContains(response, "No membership period has been created.")

    def test_with_period(self):
        MembershipPeriod.objects.create(short_name="2018-2019", start=timezone.now())

        response = self.test_logged_in_admin()
        self.assertContains(response, "Membership application - 2018-2019")

    def test_user_with_current_membership(self):
        user = self.create_user_and_log_in(verified=True, admin=True)
        period = MembershipPeriod.objects.create(short_name="2018-2019", start=timezone.now())
        Membership.objects.create(period=period, user_profile=user.profile)

        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)

        self.assertContains(response, "You already applied to the period 2018-2019. Current status: Pending approval")

    def test_post_invalid_form(self):
        self.create_user_and_log_in(verified=True, admin=True)
        MembershipPeriod.objects.create(short_name="2018-2019", start=timezone.now())
        response = self.do_POST({})
        self.assertContains(response, "Your application is invalid")

    def test_post_valid_form(self):
        user = self.create_user_and_log_in(verified=True, admin=True)
        period = MembershipPeriod.objects.create(short_name="2018-2019", start=timezone.now())

        response = self.do_POST({"period_id": period.id,
                                 "user_id": user.id,
                                 "public_statement": "My statement",
                                 "agree_membership": True})
        self.assertContains(response, "Your application was sent successfully and will be reviewed shortly")


class MembershipPeriodCreateViewTests(TestCase, GuiViewMixin):
    def setUp(self):
        self.setUpGuiTests(reverse('membership-period-create'), superuser_needed=True)

    @patch('gui.models.MembershipPeriod.current_period',
           return_value=MagicMock(short_name="2042-2043", description="Period description",
                                  agreement_url="https://x.org/agreement.pdf",
                                  start=timezone.now()))
    def test_default_values(self, period_mocked):
        self.create_user_and_log_in(verified=True, admin=True)
        response = self.client.get(self.url, {})
        self.assertContains(response, period_mocked.return_value.short_name)
        self.assertContains(response, period_mocked.return_value.description)
        self.assertContains(response, period_mocked.return_value.agreement_url)
        self.assertContains(response, period_mocked.return_value.start.strftime("%Y-%m-%d %H:%M:%S"))

    def test_post_invalid_form(self):
        self.create_user_and_log_in(verified=True, admin=True)
        response = self.client.post(self.url, {})
        self.assertEqual(response.request.get('PATH_INFO'), self.url)
        self.assertContains(response, "has-error")

    def test_post_valid_form(self):
        self.create_user_and_log_in(verified=True, admin=True)
        response = self.client.post(self.url, {"short_name": "My period",
                                               'description': "My description",
                                               "agreement_url": "https://x.org",
                                               "start": '2018-01-01'})
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, reverse('index'))


class ViewMembershipApprovalTests(TestCase, GuiViewMixin):
    def setUp(self):
        self.setUpGuiTests(reverse('membership-approval'), superuser_needed=True)

    def test_post_non_integer_ids(self):
        self.create_user_and_log_in(admin=True, verified=True)
        response = self.do_POST({'ids': ["0", "1", "2", "a", "d"]})
        self.assertContains(response, "ERROR: One or more IDs are not integers...")

    def test_post_invalid_action(self):
        self.create_user_and_log_in(admin=True, verified=True)
        response = self.do_POST({'ids': [], "action": "invalid"})
        self.assertContains(response, "ERROR: Unsupported action...")

    def test_post_approve_action(self):
        user = self.create_user_and_log_in(admin=True, verified=True)
        period1 = MembershipPeriod.objects.create(short_name="2017-2018", start=timezone.now())
        period2 = MembershipPeriod.objects.create(short_name="2018-2019", start=timezone.now())
        membership1 = Membership.objects.create(period=period1, user_profile=user.profile)
        membership2 = Membership.objects.create(period=period2, user_profile=user.profile)

        response = self.do_POST({'ids': [membership1.id, membership2.id], "action": "approve"})
        self.assertContains(response, "You approved 2 membership applications")

    def test_post_reject_action_no_reasons(self):
        user = self.create_user_and_log_in(admin=True, verified=True)
        period1 = MembershipPeriod.objects.create(short_name="2017-2018", start=timezone.now())
        period2 = MembershipPeriod.objects.create(short_name="2018-2019", start=timezone.now())
        membership1 = Membership.objects.create(period=period1, user_profile=user.profile)
        membership2 = Membership.objects.create(period=period2, user_profile=user.profile)

        response = self.do_POST({'ids': [membership1.id, membership2.id], "action": "reject"})
        self.assertContains(response, "Can&#39;t refuse  &lt;user@provider.com&gt;&#39;s application without a reason")

    def test_post_reject_action_with_reasons(self):
        user = self.create_user_and_log_in(admin=True, verified=True)
        period1 = MembershipPeriod.objects.create(short_name="2017-2018", start=timezone.now())
        period2 = MembershipPeriod.objects.create(short_name="2018-2019", start=timezone.now())
        membership1 = Membership.objects.create(period=period1, user_profile=user.profile)
        membership2 = Membership.objects.create(period=period2, user_profile=user.profile)

        response = self.do_POST({'ids': [membership1.id, membership2.id],
                                 'reject_reason_{}'.format(membership1.id): "Reason 1",
                                 'reject_reason_{}'.format(membership2.id): "Reason 2",
                                 "action": "reject"})
        self.assertContains(response, "You rejected 2 membership applications")


class ViewAboutTests(TestCase, GuiViewMixin):
    def setUp(self):
        self.setUpGuiTests(reverse('about'))
